package bojun

import (
	"errors"
	"fmt"

	"strconv"

	"github.com/tidwall/gjson"
)

//条码检测
//传入 productCode[货品条码],查询该货品条码是否存在于伯俊系统中
//@Param  productCode string
//@Return bool error
func (api *BurgeonConnection) CheckProductAlias(productCode string) error {
	postData := api.NewQuery()
	postData.QuerySetTable("M_PRODUCT_ALIAS")
	postData.QuerySetStartRange(0, 100)
	postData.QuerySetCondition("NO", productCode)
	postData.QuerySetResult("NO")
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}

	jsParse := gjson.Parse(jsResponse)
	if jsParse.Get("0.rows.#").Num == 0 {
		return errors.New("条码:" + productCode + "不存在")
	}
	return nil
}

//条码检测
//传入 productCodes[货品条码切片],查询这些货品条码是否存在于伯俊系统中
//若有一个不在,则返回 flase
//@Param  productCode string
//@Return bool error
func (api *BurgeonConnection) CheckProductAliasMore(productCodes []ReTailItem) error {
	for _, v := range productCodes {
		err := api.CheckProductAlias(v.Name)
		if err != nil {
			return err
		}
	}
	return nil
}

//条码检测
//传入 productCodes[货品条码切片],查询这些货品条码是否存在于伯俊系统中
//若有一个不在,则返回 flase
//@Param  productCode string
//@Return bool error
func (api *BurgeonConnection) CheckProductStorange(storeCode string, products []ReTailItem) error {
	for _, v := range products {
		//检查销售类型
		if v.Type == ReTailBack {
			continue
		}
		num, err := api.GetStoreStorage(storeCode, v.Name)
		if err != nil {
			return err
		}
		if num < v.Qty {
			return errors.New(fmt.Sprintf("%s的库存为:%d,销售数量为:%d,请修改数量", v.Name, num, v.Qty))
		}
	}
	return nil
}

//检查对应店是否有对应营业员
//仅当该店存在该营业员返回true,其余都是false
//@parm store_code 店仓编号
//@parm user_code 营业员编号
func (api *BurgeonConnection) CheckUserStore(storeCode, userCode string) error {
	postData := api.NewQuery()
	postData.QuerySetTable("HR_EMPLOYEE_STORE")
	postData.QuerySetResult("HR_EMPLOYEE_ID;NO", "C_STORE_ID;CODE")
	postData.QuerySetStartRange(0, 10)
	postData.QuerySetConditionCombine("and")
	postData.QuerySetConditionExpr1("C_STORE_ID;CODE", storeCode)
	postData.QuerySetConditionExpr2("HR_EMPLOYEE_ID;NO", userCode)
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	jsParse := gjson.Parse(jsResponse)
	if jsParse.Get("0.rows.#").Num == 0 {
		return errors.New("店仓:" + storeCode + "中没有营业员:" + userCode)
	}
	return nil

}

//营业员检测
//传入 user_code[营业员编号],查询该营业员是否存在于伯俊系统中
//@Param  user_code string
//@Return bool error
func (api *BurgeonConnection) CheckUser(userCode string) error {
	postData := api.NewQuery()
	postData.QuerySetTable("HR_EMPLOYEE")
	postData.QuerySetResult("NO")
	postData.QuerySetStartRange(0, 10)
	postData.QuerySetConditionCombine("and")
	postData.QuerySetConditionExpr1("ISACTIVE", "Y")
	postData.QuerySetConditionExpr2("NO", userCode)
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	jsParse := gjson.Parse(jsResponse)
	if jsParse.Get("o.rows.#").Num == 0 {
		return errors.New("营业员:" + userCode + "不存在")
	}
	return nil
}

//店仓检测
//传入 store_code[店仓编号],查询该店仓是否存在于伯俊系统中
//@Param  user_code string
//@Return bool error
func (api *BurgeonConnection) CheckStore(store_code string) error {
	postData := api.NewQuery()
	postData.QuerySetTable("C_STORE")
	postData.QuerySetResult("CODE")
	postData.QuerySetStartRange(0, 10)
	postData.QuerySetConditionCombine("and")
	postData.QuerySetConditionExpr1("ISACTIVE", "Y")
	postData.QuerySetConditionExpr2("CODE", store_code)
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	jsParse := gjson.Parse(jsResponse)
	if jsParse.Get("o.rows.#").Num == 0 {
		return errors.New(store_code + "店仓不存在")
	}
	return nil
}

//rs0 条码有错
//rs1 店仓有错
//rs2 店员
//rs3 店员所属店仓
//rs4 店仓盘点日期
//rs5条码库存 是否满足
func (api *BurgeonConnection) CheckAll(store_code, user_code, retailData string, products []ReTailItem) (rs0, rs1, rs2, rs3, rs4, rs5 error) {
	//检测店仓
	rs1 = api.CheckStore(store_code)
	if rs1 != nil {
		return
	}
	//检测店仓盘点日期
	rs4 = api.CheckStoreDateBlock(store_code, retailData)
	if rs4 != nil {
		return
	}
	//检测用户
	rs2 = api.CheckUser(user_code)
	if rs2 != nil {
		return
	}
	//检测用户所属店仓
	rs3 = api.CheckUserStore(store_code, user_code)
	if rs3 != nil {
		return
	}
	//检测条码
	rs0 = api.CheckProductAliasMore(products)
	if rs0 != nil {
		return
	}
	//检测条码库存
	rs5 = api.CheckProductStorange(store_code, products)
	return
}

//VIP检测
//传入 vipcard[vip卡号],查询该vip是否存在于伯俊系统中
//@Param  vipcard string
//@Return bool error
func (api *BurgeonConnection) CheckVip(vipcard string) (exist bool, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_VIP")
	postData.QuerySetStartRange(0, 100)
	postData.QuerySetCondition("CARDNO", vipcard)
	postData.QuerySetResult("CARDNO")
	jsonResultStr, err := api.post(postData)
	if err == nil {
		return BurgeonRowsExists(jsonResultStr), nil
	} else {
		return false, err
	}
}

func (api *BurgeonConnection) CheckStoreDateBlock(storCode, retailData string) (err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_STORE")
	postData.QuerySetCondition("CODE", storCode)
	postData.QuerySetResult("DATEBLOCK")
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	jsParse := gjson.Parse(jsResponse)
	dateBlock := jsParse.Get("0.rows.0.0").String()
	if len(dateBlock) == 0 {
		return errors.New(fmt.Sprintf("店仓[%s]盘点日期为空,不予以处理", storCode))
	}
	retailDataNum, _ := strconv.Atoi(retailData)
	dateBlockNum, _ := strconv.Atoi(dateBlock)
	if dateBlockNum < retailDataNum {
		return nil
	} else {
		return errors.New(fmt.Sprintf("店仓[%s]盘点日期大于等于单据日期", storCode))
	}

}

//通过店仓库存查询
func (api *BurgeonConnection) GetStoreStorage(storId string, productCode string) (int64, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("V_FA_V_STORAGE1")
	postData.QuerySetResult("QTY")
	postData.QuerySetStartRange(0, 10)
	postData.QuerySetConditionCombine("AND")
	postData.QuerySetConditionExpr1("C_STORE_ID;CODE", storId)
	postData.QuerySetConditionExpr2("M_PRODUCTALIAS_ID;NO", productCode)
	jsResponse, err := api.post(postData)
	if err != nil {
		return 0, err
	}
	return gjson.Get(jsResponse, "0.rows.0.0").Int(), nil
}
