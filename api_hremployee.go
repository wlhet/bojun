package bojun

import (
	"github.com/tidwall/gjson"
)

//获取营业员信息 工号 姓名  店仓  进销商
//@param nunber string 营业员编号
//@return burgeon.Employee 营业员详细信息[工号 姓名  店仓  进销商]
func (api *BurgeonConnection) GetemployeesInfo(nunber string) (employee Employee, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_V_EMPLOYEE")
	postData.QuerySetCondition("NO", nunber)
	postData.QuerySetResult("NAME", "NO", "C_STORE_ID;NAME", "C_CUSTOMER_ID;NAME", "ID")
	var emp Employee
	jsResponse, err := api.post(postData)
	if err != nil {
		return emp, err
	}

	jsParse := gjson.Parse(jsResponse)
	if !jsParse.Get("0.rows.0.1").Exists() {
		return emp, ErrNoDataFound
	}
	emp.Name = jsParse.Get("0.rows.0.0").String()
	emp.WorkNo = jsParse.Get("0.rows.0.1").String()
	emp.Store = jsParse.Get("0.rows.0.2").String()
	emp.Customer = jsParse.Get("0.rows.0.3").String()
	emp.Id = jsParse.Get("0.rows.0.4").Int()
	return emp, nil

}

//获取营业员所有所属店仓
//@param nunber string 营业员编号
//@return []string 营业员所在店仓名字,可为多个店仓
func (api *BurgeonConnection) GetemployeesStores(nunber string) (stores []string, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("HR_EMPLOYEE_STORE")
	postData.QuerySetCondition("HR_EMPLOYEE_ID;NO", nunber)
	postData.QuerySetResult("C_STORE_ID;NAME")
	store := make([]string, 0)
	jsResponse, err := api.post(postData)
	if err != nil {
		return store, err
	}
	jsParse := gjson.Parse(jsResponse)
	if jsParse.Get("0.rows.#").Num == 0 {
		return nil, ErrNoDataFound
	}
	array := jsParse.Get("0.rows.#.0").Array()
	for _, v := range array {
		store = append(store, v.String())
	}
	return store, nil

}

//新增员工信息
//@param nunber string 营业员编号
//@param name   string 营业员姓名
//@param store  string 店仓编号或者店仓名
//@param customer string 经销商编号或经销商名
//@param phone string 营业员手机号
//@return err   error  是否成功新增
func (api *BurgeonConnection) AddEmployees(nunber, name, store, customer, phone string) error {
	postData := api.NewObjectCreate()
	postData.ObjectCreateSetTable("C_V_EMPLOYEE")
	postData.ObjectCreateSetColumn("NO", nunber)
	postData.ObjectCreateSetColumn("NAME", name)
	postData.ObjectCreateSetColumn("C_STORE_ID__NAME", store)
	postData.ObjectCreateSetColumn("C_CUSTOMER_ID__NAME", customer)
	postData.ObjectCreateSetColumn("HANDSET", phone)
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}
