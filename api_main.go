package bojun

import (
	"fmt"
)

var servers = make(map[string]*BurgeonConnection)
var mpServers map[string][]string
var ErrNoDataFound = fmt.Errorf("No Data Found")

/*
this package is the main api of burgeon;
all public api are in this package

how to use:
	api:=burgeon.NewApi(AppId, AppKey, ApiUrl)
	api.Func()
*/

//@Param  bg *burgeon.BurgeonConnection
//@Return *Api
func NewApi(appId, appKey, host string, port int64) *BurgeonConnection {
	return newBurgeonConnection(appId, appKey, host, port)
}

//向外单独提供
func newBurgeonConnection(appId, appKey, host string, port int64) *BurgeonConnection {
	url := fmt.Sprintf("http://%s:%d/servlets/binserv/Rest", host, port)
	return &BurgeonConnection{appId, appKey, url}
}
func RegDefaultApis(svs map[string]*BurgeonConnection) {
	servers = svs
}

func RegDefaultApi(brand, appId, appKey, host string, port int64) {
	servers[brand] = newBurgeonConnection(appId, appKey, host, port)
}

func GetApp(brand string) (*BurgeonConnection, error) {
	app, ok := servers[brand]
	if !ok {
		return nil, fmt.Errorf("Api Not Found")
	}
	return app, nil
}
