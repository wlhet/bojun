package bojun

type pandianType int64

func (api *BurgeonConnection) NewPanDianSome(data string, storeName, remark string, items []PanDianItem) error {
	return api.newPanDian(data, storeName, remark, 1, items)
}
func (api *BurgeonConnection) NewPanDianAll(data string, storeName, remark string, items []PanDianItem) error {
	return api.newPanDian(data, storeName, remark, 2, items)
}

/*
新增盘点单
*/
func (api *BurgeonConnection) newPanDian(data string, storeName, remark string, t int64, items []PanDianItem) error {
	postData := api.NewProcessOrder()
	postData.ProcessOrderMasterObjSetTable("M_INVENTORY")
	postData.ProcessOrderIfSubmit(false)
	postData.ProcessOrderMasterObjSetColumn("C_STORE_ID__NAME", storeName) //店名
	postData.ProcessOrderMasterObjSetColumn("BILLDATE", data)              //盘点时间
	switch t {                                                             //INH:历史盘点,IHS:历史抽盘,
	case 1:
		postData.ProcessOrderMasterObjSetColumn("DOCTYPE", "IHS") //历史抽盘
	case 2:
		postData.ProcessOrderMasterObjSetColumn("DOCTYPE", "INH") //历史盘点
	}
	postData.ProcessOrderMasterObjSetColumn("IS_BAS", "N")         //是否按箱
	postData.ProcessOrderMasterObjSetColumn("DESCRIPTION", remark) //是否按箱
	postData.ProcessOrderDetailObjsSetTables("M_INVENTORY_SHELFITEM")
	listItems := make([]map[string]interface{}, 0)
	for _, v := range items {
		listItems = append(listItems, map[string]interface{}{
			"SHELFNO":                       v.ShefnNo,
			"M_PRODUCT_ID__NAME":            v.Sku,
			"QTY":                           v.Qty,
			"M_ATTRIBUTESETINSTANCE_ID__ID": "",
		})
	}
	postData.ProcessOrderDetailObjsAddrefobjs("M_INVENTORY_SHELFITEM", listItems...)
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

//
