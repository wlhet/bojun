package bojun

import (
	"crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/tidwall/gjson"
)

/*

	bg := burgeon.NewBurgeonConnection("nea@burgeon.com.cn", "123", "http://xxx.xxxx.com:port/servlets/binserv/Rest")
	rs, _ := bg.AddNewMRetail("9999", "9999", "测试", "20210318",
		burgeon.ReTailItem{Name: "723763288132", Price: 1000.0, Qty: 10, IsSwap: "N", Type: 2},
		burgeon.ReTailItem{Name: "723763291965", Price: 1000.0, Qty: 10, IsSwap: "N", Type: 1})
	fmt.Println(rs)

*/

type RetailItem struct {
	Id      int64
	Vip     string
	Sku     string
	Qty     int64
	Rqty    int64
	SalerNo string
}

//新增零售单
//@param store       string 店仓编号或者店仓名
//@param saler       string 营业员编号
//@param remark      string 零售单备注
//@param billdate    string 零售单日期
//@param retailitems []burgeon.ReTailItem 零售行明细
//@param payitems    []burgeon.PayItem    零售付款方式
//@return retail     string 成功返回单据编号,失败返回空串
func (api *BurgeonConnection) AddNewMRetail(submit bool, store, saler, remark, billdate string, vipcard string, is_intl string, retailitems []ReTailItem, payitems []PayItem) (retail string, err error) {
	postData := api.NewProcessOrder()
	postData.ProcessOrderMasterObjSetTable("M_RETAIL")
	postData.ProcessOrderIfSubmit(submit)
	postData.ProcessOrderMasterObjSetColumn("C_STORE_ID__NAME", store)
	if vipcard != "" {
		postData.ProcessOrderMasterObjSetColumn("C_VIP_ID__CARDNO", vipcard)
	}
	if is_intl != "" {
		postData.ProcessOrderMasterObjSetColumn("ISINTL", is_intl)
	}
	if saler != "" {
		postData.ProcessOrderMasterObjSetColumn("SALESREP_ID__NO", saler)
	}
	postData.ProcessOrderMasterObjSetColumn("DESCRIPTION", remark)
	postData.ProcessOrderMasterObjSetColumn("BILLDATE", billdate)
	list := make([]map[string]interface{}, 0)
	if len(retailitems) == 0 {
		return EmptyString, errors.New("No Items Error")
	}
	for _, v := range retailitems {
		list = append(list, map[string]interface{}{
			"M_PRODUCT_ID__NAME": v.Name,
			"PRICEACTUAL":        v.Price,
			"QTY":                v.Qty,
			"TYPE":               v.Type,
			"IS_SWAP":            v.IsSwap,
			"DESCRIPTION":        v.Remark,
		})
	}
	postData.ProcessOrderDetailObjsAddrefobjs("M_RETAILITEM", list...)
	if len(payitems) > 0 { //有付款方式,需要增加付款方式子表头
		postData.ProcessOrderDetailObjsSetTables("M_RETAILITEM", "M_RETAILPAYITEM") //子表列表中增付款方式表 注意有顺序限制
		listPay := make([]map[string]interface{}, 0)
		for _, v := range payitems {
			listPay = append(listPay, map[string]interface{}{
				"C_PAYWAY_ID__NAME": v.Type,
				"BASE_PAYAMOUNT":    0,
				"PAYAMOUNT":         v.PayAmt,
			})
		}
		postData.ProcessOrderDetailObjsAddrefobjs("M_RETAILPAYITEM", listPay...)
	} else { //没有付款方式,只处理零售明细即可
		postData.ProcessOrderDetailObjsSetTables("M_RETAILITEM")
	}
	jsResponse, err := api.post(postData)
	if err != nil {
		switch err.Error() {
		case "行 1: 输入的数据已存在:店仓+创建时间+POS零售单号":
			randint, _ := rand.Int(rand.Reader, big.NewInt(5))
			time.Sleep(time.Duration(randint.Int64()) * time.Second)
			return api.AddNewMRetail(submit, store, saler, remark, billdate, "", "", retailitems, payitems)
		default:
			return "", err
		}
	}
	jsParse := gjson.Parse(jsResponse)
	if submit { //提交直接返回单号
		retail = jsParse.Get("0.message").String()
	} else { //未提交需要自行读取单号
		objId := jsParse.Get("0.objectid").String()
		retail, _ = api.GetrRetailNoById(objId)
	}
	return strings.ReplaceAll(retail, "提交成功！！", ""), nil
}

func (api *BurgeonConnection) AddNewMRetailBack(submit bool, store, saler, remark, billdate string, vipcard string, is_intl string, retailitems []ReTailItem, payitems []PayItem) (retail string, err error) {
	postData := api.NewProcessOrder()
	postData.ProcessOrderMasterObjSetTable("M_RETAIL")
	postData.ProcessOrderIfSubmit(submit)
	postData.ProcessOrderMasterObjSetColumn("C_STORE_ID__NAME", store)
	if vipcard != "" {
		postData.ProcessOrderMasterObjSetColumn("C_VIP_ID__CARDNO", vipcard)
	}
	if is_intl != "" {
		postData.ProcessOrderMasterObjSetColumn("ISINTL", is_intl)
	}
	postData.ProcessOrderMasterObjSetColumn("SALESREP_ID__NO", saler)
	postData.ProcessOrderMasterObjSetColumn("DESCRIPTION", remark)
	postData.ProcessOrderMasterObjSetColumn("BILLDATE", billdate)
	postData.ProcessOrderMasterObjSetColumn("REFNO", api.GetRefNo())

	list := make([]map[string]interface{}, 0)
	if len(retailitems) == 0 {
		return EmptyString, errors.New("NowItemsErrr")
	}
	for _, v := range retailitems {
		list = append(list, map[string]interface{}{
			"M_PRODUCT_ID__NAME":  v.Name,
			"PRICEACTUAL":         v.Price,
			"QTY":                 v.Qty,
			"TYPE":                v.Type,
			"IS_SWAP":             v.IsSwap,
			"ORGDOCNO":            v.DocNo,
			"M_RETAILITEM_ID__ID": v.RetailItemId,
		})
	}
	postData.ProcessOrderDetailObjsAddrefobjs("M_RETAILITEM", list...)
	if len(payitems) > 0 { //有付款方式,需要增加付款方式子表头
		postData.ProcessOrderDetailObjsSetTables("M_RETAILITEM", "M_RETAILPAYITEM") //子表列表中增付款方式表 注意有顺序限制
		listPay := make([]map[string]interface{}, 0)
		for _, v := range payitems {
			listPay = append(listPay, map[string]interface{}{
				"C_PAYWAY_ID__NAME": v.Type,
				"BASE_PAYAMOUNT":    0,
				"PAYAMOUNT":         v.PayAmt,
			})
		}
		postData.ProcessOrderDetailObjsAddrefobjs("M_RETAILPAYITEM", listPay...)
	} else { //没有付款方式,只处理零售明细即可
		postData.ProcessOrderDetailObjsSetTables("M_RETAILITEM")
	}
	jsResponse, err := api.post(postData)
	if err != nil {
		switch err.Error() {
		case "行 1: 输入的数据已存在:店仓+创建时间+POS零售单号":
			randint, _ := rand.Int(rand.Reader, big.NewInt(5))
			time.Sleep(time.Duration(randint.Int64()) * time.Second)
			return api.AddNewMRetailBack(submit, store, saler, remark, billdate, "", "", retailitems, payitems)
		default:
			return "", err
		}
	}
	jsParse := gjson.Parse(jsResponse)
	if submit { //提交直接返回单号
		retail = jsParse.Get("0.message").String()
	} else { //未提交需要自行读取单号
		objId := jsParse.Get("0.objectid").String()
		retail, _ = api.GetrRetailNoById(objId)
	}
	return strings.ReplaceAll(retail, "提交成功！！", ""), nil
}

func (api *BurgeonConnection) GetRefNo() string {
	now := time.Now()
	rs := GetRandomString(3)
	return strings.ToUpper(fmt.Sprintf("weimob_%s_%s", rs, now.Format("20060102150405")))
}

//通过零售单Id获取单号
func (api *BurgeonConnection) GetrRetailNoById(id string) (no string, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("M_RETAIL")
	postData.QuerySetCondition("ID", id)
	postData.QuerySetResult("DOCNO")
	jsResponse, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	}
	jsParse := gjson.Parse(jsResponse)
	if jsParse.Get("0.rows.#").Num == 0 {
		return EmptyString, ErrNoDataFound
	}
	return jsParse.Get("0.rows.0.0").String(), nil
}

//新增零售单 针对于开启多营业员的情况  还是只填写一个营业员,只是多了几个字段
//@param store       string 店仓编号或者店仓名
//@param saler       string 营业员编号
//@param remark      string 零售单备注
//@param billdate    string 零售单日期
//@param retailitems []burgeon.ReTailItem 零售行明细
//@param payitems    []burgeon.PayItem    零售付款方式
//@return retail     string 成功返回单据编号,失败返回空串
func (api *BurgeonConnection) AddNewMRetailWithOpenMoreSaler(submit bool, store, saler, remark, billdate string, retailitems []ReTailItem, payitems []PayItem) (retail string, err error) {
	postData := api.NewProcessOrder()
	postData.ProcessOrderMasterObjSetTable("M_RETAIL")
	postData.ProcessOrderIfSubmit(submit)
	postData.ProcessOrderMasterObjSetColumn("C_STORE_ID__NAME", store)
	postData.ProcessOrderMasterObjSetColumn("SALESREP_ID__NO", saler)
	postData.ProcessOrderMasterObjSetColumn("ISINTL", "N")
	postData.ProcessOrderMasterObjSetColumn("DESCRIPTION", remark)
	postData.ProcessOrderMasterObjSetColumn("BILLDATE", billdate)
	list := make([]map[string]interface{}, 0)
	if len(retailitems) == 0 {
		return EmptyString, errors.New("NowItemsErrr")
	}
	emp, _ := api.GetemployeesInfo(saler)
	for _, v := range retailitems {
		list = append(list, map[string]interface{}{
			"M_PRODUCT_ID__NAME": v.Name,
			"PRICEACTUAL":        v.Price,
			"QTY":                v.Qty,
			"TYPE":               v.Type,
			"IS_SWAP":            v.IsSwap,
			"SALESREPS_ID":       emp.Id,
			"SALESREPS_NAME":     emp.Name,
			"SALESREPS_RATE":     "1.0",
		})
	}

	postData.ProcessOrderDetailObjsAddrefobjs("M_RETAILITEM", list...)
	if len(payitems) > 0 { //有付款方式,需要增加付款方式子表头
		postData.ProcessOrderDetailObjsSetTables("M_RETAILITEM", "M_RETAILPAYITEM") //子表列表中增付款方式表 注意有顺序限制
		listPay := make([]map[string]interface{}, 0)
		for _, v := range payitems {
			listPay = append(listPay, map[string]interface{}{
				"C_PAYWAY_ID__NAME": v.Type,
				"BASE_PAYAMOUNT":    0,
				"PAYAMOUNT":         v.PayAmt,
			})
		}
		postData.ProcessOrderDetailObjsAddrefobjs("M_RETAILPAYITEM", listPay...)
	} else { //没有付款方式,只处理零售明细即可
		postData.ProcessOrderDetailObjsSetTables("M_RETAILITEM")
	}
	jsResponse, err := api.post(postData)
	if err != nil {
		switch err.Error() {
		case "行 1: 输入的数据已存在:店仓+创建时间+POS零售单号":
			randint, _ := rand.Int(rand.Reader, big.NewInt(5))
			time.Sleep(time.Duration(randint.Int64()) * time.Second)
			return api.AddNewMRetail(submit, store, saler, remark, billdate, "", "", retailitems, payitems)
		default:
			return EmptyString, err
		}
	}
	jsParse := gjson.Parse(jsResponse)
	if submit { //提交直接返回单号
		retail = jsParse.Get("0.message").String()
	} else { //未提交需要自行读取单号
		objId := jsParse.Get("0.objectid").String()
		retail, _ = api.GetrRetailNoById(objId)
	}
	return strings.ReplaceAll(retail, "提交成功！！", ""), nil

}

//新增调拨单据
func (api *BurgeonConnection) AddNewTrans(billdate string, orig_store, dest_store string, remark string, item []TransItem) (string, error) {
	postData := api.NewProcessOrder()
	postData.ProcessOrderMasterObjSetTable("M_TRANSFER")
	postData.ProcessOrderIfSubmit(true)
	postData.ProcessOrderMasterObjSetColumn("BILLDATE", billdate)
	postData.ProcessOrderMasterObjSetColumn("TRANSFERCLASS", "JTR") //调拨类别 JTR:急调,PTR:平调,
	postData.ProcessOrderMasterObjSetColumn("TRANTYPE", 1)
	postData.ProcessOrderMasterObjSetColumn("C_ORIG_ID__NAME", orig_store) //发货店
	postData.ProcessOrderMasterObjSetColumn("C_DEST_ID__NAME", dest_store) //收货店
	postData.ProcessOrderMasterObjSetColumn("DESCRIPTION", remark)
	postData.ProcessOrderDetailObjsSetTables("M_TRANSFERITEM")

	list := make([]map[string]interface{}, 0)
	for _, v := range item {
		tran := make(map[string]interface{})
		tran["C_STORE_ID__NAME"] = v.StoreName
		tran["M_PRODUCT_ID__NAME"] = v.ProducntNmae
		tran["QTY"] = v.Qty
		tran["M_ATTRIBUTESETINSTANCE_ID__ID"] = v.AttrId
		list = append(list, tran)
	}
	postData.ProcessOrderDetailObjsAddrefobjs("M_TRANSFERITEM ", list...)
	jsResponse, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	}
	jsParse := gjson.Parse(jsResponse)
	code := jsParse.Get("0.code").Int()
	msg := jsParse.Get("0.message").String()
	if code != 0 {
		return EmptyString, fmt.Errorf(msg)
	}
	return strings.ReplaceAll(msg, "提交成功！！", ""), nil
}

func (api *BurgeonConnection) GetSkuInfoBySkuCode(sku string) (string, int64, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("微盟通过条码查询货号及属性ID")
	postData.ExecuteSqlSetValues(sku)
	jsResponse, err := api.post(postData)
	if err != nil {
		return EmptyString, -1, err
	}
	jsParse := gjson.Parse(jsResponse)
	product := jsParse.Get("0.result.0.0").String()
	infoId := jsParse.Get("0.result.0.1").Int()
	if product != EmptyString && infoId != 0 {
		return product, infoId, nil
	}
	return EmptyString, -1, ErrNoDataFound
}

func (api *BurgeonConnection) GetVipCardByRetailNo(docno string) (string, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("零售单号获取零售单ID及VIP卡号")
	postData.ExecuteSqlSetValues(docno)
	jsResponse, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	}
	jsParse := gjson.Parse(jsResponse)
	code := jsParse.Get("0.code").Int()
	msg := jsParse.Get("0.message").String()
	if code != 0 {
		return EmptyString, fmt.Errorf(msg)
	}
	list := jsParse.Get("0.result").Array()
	if len(list) == 0 {
		return EmptyString, ErrNoDataFound
	}
	card := jsParse.Get("0.result.0.0").String()
	return card, nil
}

func (api *BurgeonConnection) GetRetailItemInfoByRetailNo(docno string) ([]RetailItem, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("通过零售单号获取零售明明细")
	postData.ExecuteSqlSetValues(docno)
	jsResponse, err := api.post(postData)
	if err != nil {
		return nil, err
	}
	jsParse := gjson.Parse(jsResponse)
	code := jsParse.Get("0.code").Int()
	msg := jsParse.Get("0.message").String()
	if code != 0 {
		return nil, fmt.Errorf(msg)
	}
	list := jsParse.Get("0.result").Array()
	if len(list) == 0 {
		return nil, ErrNoDataFound
	}
	results := make([]RetailItem, 0)
	for _, v := range list {
		var item RetailItem
		vv := v.Array()
		item.Id = vv[0].Int()
		item.Qty = vv[1].Int()
		item.Rqty = vv[2].Int()
		item.Sku = vv[3].String()
		item.SalerNo = vv[4].String()
		results = append(results, item)
	}
	return results, nil
}

//测试调拨单//在同一个操作里面完成
func (api *BurgeonConnection) TestMuitiDo(docno string) ([]RetailItem, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("通过零售单号获取零售明明细")
	postData.ExecuteSqlSetValues(docno)
	jsResponse, err := api.post(postData)
	if err != nil {
		return nil, err
	}
	jsParse := gjson.Parse(jsResponse)
	code := jsParse.Get("0.code").Int()
	msg := jsParse.Get("0.message").String()
	if code != 0 {
		return nil, fmt.Errorf(msg)
	}
	list := jsParse.Get("0.result").Array()
	if len(list) == 0 {
		return nil, ErrNoDataFound
	}
	results := make([]RetailItem, 0)
	for _, v := range list {
		var item RetailItem
		vv := v.Array()
		item.Id = vv[0].Int()
		item.Qty = vv[1].Int()
		item.Rqty = vv[2].Int()
		item.Sku = vv[3].String()
		item.SalerNo = vv[4].String()
		results = append(results, item)
	}
	return results, nil
}
