package bojun

import (
	"fmt"

	"github.com/tidwall/gjson"
)

var defultStore = make(map[string]string)

//获取店仓日结日期
func (api *BurgeonConnection) GetStoreChkDay(store string) (chkday string, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_STORE")
	postData.QuerySetCondition("CODE", store)
	postData.QuerySetResult("CHKDAY")
	js_string, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	} else {
		dateString := gjson.Get(js_string, "0.rows.0.0").String()
		if dateString == "null" {
			return "", nil
		} else {
			return dateString, nil
		}

	}
}

//修改店仓日结
func (api *BurgeonConnection) ChangeStoreChkDay(storeId string, dateString string) error {
	postData := api.NewObjectModify()
	postData.ObjectModifySetTable("C_STORE")
	postData.ObjectModifySetColumn("id", storeId)
	postData.ObjectModifySetColumn("CHKDAY", dateString)
	postData.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

//获取店仓盘点日期
func (api *BurgeonConnection) GetStoreDateBlock(storeCode string) (chkday string, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_STORE")
	postData.QuerySetCondition("CODE", storeCode)
	postData.QuerySetResult("DATEBLOCK")
	js_string, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	} else {
		dateString := gjson.Get(js_string, "0.rows.0.0").String()
		if dateString == "null" {
			return "", nil
		} else {
			return dateString, nil
		}

	}
}

//获取店仓名通过编号
func (api *BurgeonConnection) GetStoreNameByCode(storeCode string) (name string, err error) {
	name, ok := defultStore[storeCode]
	if ok {
		return name, nil
	}
	postData := api.NewQuery()
	postData.QuerySetTable("C_STORE")
	postData.QuerySetCondition("CODE", storeCode)
	postData.QuerySetResult("NAME")
	js_string, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	} else {
		dateString := gjson.Get(js_string, "0.rows.0.0").String()
		if dateString == "null" {
			return "", fmt.Errorf("通过店仓编号查询店名失败")
		} else {
			defultStore[storeCode] = dateString
			return dateString, nil

		}

	}
}

//修改店仓盘点日期
func (api *BurgeonConnection) ChangeStoreDateBlock(storeId string, dateString string) error {
	postData := api.NewObjectModify()
	postData.ObjectModifySetTable("C_V_STORE_OTH")
	postData.ObjectModifySetColumn("id", storeId)
	postData.ObjectModifySetColumn("DATEBLOCK", dateString)
	postData.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

//通过店仓编码获取店仓Id
func (api *BurgeonConnection) GetStoreIdByCode(storCode string) (storeId string, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_STORE")
	postData.QuerySetCondition("CODE", storCode)
	postData.QuerySetResult("ID")
	js_string, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	} else {
		dateString := gjson.Get(js_string, "0.rows.0.0").String()
		return dateString, nil

	}
}

//通过店仓编码获取店仓Id,日结日期,盘点日期
func (api *BurgeonConnection) GetStoreIdChkDayDateBlock(storCode string) (storeId, chkDay, dateBlock string, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_STORE")
	postData.QuerySetCondition("CODE", storCode)
	postData.QuerySetResult("ID", "CHKDAY", "DATEBLOCK")
	js_string, err := api.post(postData)
	if err != nil {
		return EmptyString, EmptyString, EmptyString, err
	} else {
		storeId = gjson.Get(js_string, "0.rows.0.0").String()
		chkDay = gjson.Get(js_string, "0.rows.0.1").String()
		dateBlock = gjson.Get(js_string, "0.rows.0.2").String()
		return

	}
}

//通过店仓编码获取门店库存
func (api *BurgeonConnection) GetStoreStack(storCode string) (storeId, chkDay, dateBlock string, err error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_STORE")
	postData.QuerySetCondition("CODE", storCode)
	postData.QuerySetResult("ID", "CHKDAY", "DATEBLOCK")
	js_string, err := api.post(postData)
	if err != nil {
		return EmptyString, EmptyString, EmptyString, err
	} else {
		storeId = gjson.Get(js_string, "0.rows.0.0").String()
		chkDay = gjson.Get(js_string, "0.rows.0.1").String()
		dateBlock = gjson.Get(js_string, "0.rows.0.2").String()
		return

	}
}

type StoreCodeStock struct {
	StoreCode string
	Sku       string
	Num       int64
}

func (api *BurgeonConnection) GetStoreStockByStoreCode(code string) ([]StoreCodeStock, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("微盟获取某店库存")
	postData.ExecuteSqlSetValues(code)
	js_string, err := api.post(postData)
	if err != nil {
		return nil, err
	}
	stocks := make([]StoreCodeStock, 0)
	list := gjson.Get(js_string, "0.result").Array()
	for _, v := range list {
		vv := v.Array()
		stocks = append(stocks, StoreCodeStock{StoreCode: vv[0].String(), Sku: vv[1].String(), Num: vv[2].Int()})
	}
	return stocks, nil
}

func (api *BurgeonConnection) GetStoreStockWithTime2Time(st, et string) ([]StoreCodeStock, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("微盟查询指定时间段库存")
	postData.ExecuteSqlSetValues(st)
	postData.ExecuteSqlSetValues(et)
	js_string, err := api.post(postData)
	if err != nil {
		return nil, err
	}
	stocks := make([]StoreCodeStock, 0)
	list := gjson.Get(js_string, "0.result").Array()
	if len(list) == 0 {
		return nil, ErrNoDataFound
	}
	for _, v := range list {
		vv := v.Array()
		stocks = append(stocks, StoreCodeStock{StoreCode: vv[0].String(), Sku: vv[1].String(), Num: vv[2].Int()})
	}
	return stocks, nil
}

func (api *BurgeonConnection) GetSkuNumByStoreIdAndSkuCode(id, sku string) (int64, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("微盟查询门店指定SKU库存")
	postData.ExecuteSqlSetValues(id)
	postData.ExecuteSqlSetValues(sku)
	js_string, err := api.post(postData)
	if err != nil {
		return -1, err
	}
	list := gjson.Get(js_string, "0.result").Array()
	if len(list) == 0 {
		return -1, ErrNoDataFound
	}
	return gjson.Get(js_string, "0.result.0.0").Int(), nil
}
