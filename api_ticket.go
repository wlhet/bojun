package bojun

func (api *BurgeonConnection) AddNewTicket(cardinfo *VipCardConfig, tkno string, card string, price int64, start, end, remark string) error {
	postData := api.NewProcessOrder()
	postData.ProcessOrderMasterObjSetTable("C_V_VOUCHERS")
	postData.ProcessOrderIfSubmit(true)
	postData.ProcessOrderMasterObjSetColumn("VOUCHERS_NO", tkno)                      //券号
	postData.ProcessOrderMasterObjSetColumn("C_VIP_ID__CARDNO", card)                 //VIP卡号
	postData.ProcessOrderMasterObjSetColumn("VOU_TYPE", "VOU5")                       //优惠券类别(购物券)
	postData.ProcessOrderMasterObjSetColumn("IS_OFFLINE", "Y")                        //是否线下
	postData.ProcessOrderMasterObjSetColumn("AMT_DISCOUNT", price)                    //优惠金额
	postData.ProcessOrderMasterObjSetColumn("START_DATE", start)                      //开始日期
	postData.ProcessOrderMasterObjSetColumn("DELTYPE", "AND")                         //排除方式
	postData.ProcessOrderMasterObjSetColumn("VALID_DATE", end)                        //过期日期
	postData.ProcessOrderMasterObjSetColumn("C_CUSTOMER_ID__NAME", cardinfo.Customer) //经销商名
	postData.ProcessOrderMasterObjSetColumn("IS_ALLSTORE", "Y")                       //是否所有店铺
	postData.ProcessOrderMasterObjSetColumn("ISSHARE_PAYTYPE", "Y")                   //付款方式是否共用
	postData.ProcessOrderMasterObjSetColumn("IS_MDAMT", "N")                          //消费时允许修改金额
	postData.ProcessOrderMasterObjSetColumn("DESCRIPTION", remark)                    //备注
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}
