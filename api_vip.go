package bojun

import (
	"errors"
	"fmt"
	"strings"

	"github.com/tidwall/gjson"
)

//获取Vip当前积分
func (api *BurgeonConnection) GetVipIntegralByCardNO(card string) (int64, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_VIP")
	postData.QuerySetCondition("CARDNO", card)
	postData.QuerySetResult("INTEGRAL")
	s, err := api.post(postData)
	if err != nil {
		return -1, err
	} else {
		result := GetBurgeonRowsArray(s)
		if len(result) == 0 {
			return -1, ErrNoDataFound
		}
		return gjson.Get(s, "0.rows.0.0").Int(), nil
	}
}

//("VIPNAME", "C_STORE_ID;CODE", "BIRTHDAY", "INTEGRAL")
func (api *BurgeonConnection) GetVipNameBirthdayIntegralByCardNO(card string) (string, string, int64, int64, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_VIP")
	postData.QuerySetCondition("CARDNO", card)
	postData.QuerySetResult("VIPNAME", "C_STORE_ID;CODE", "BIRTHDAY", "INTEGRAL")
	s, err := api.post(postData)
	if err != nil {
		return "", "", -1, -1, err
	} else {
		result := GetBurgeonRowsArray(s)
		if len(result) == 0 {
			return "", "", -1, -1, ErrNoDataFound
		}
		return gjson.Get(s, "0.rows.0.0").String(), gjson.Get(s, "0.rows.0.1").String(), gjson.Get(s, "0.rows.0.2").Int(), gjson.Get(s, "0.rows.0.3").Int(), nil
	}
}

//获取Vip等级
func (api *BurgeonConnection) GetVipLevelByCardNO(card string) (int64, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_VIP")
	postData.QuerySetCondition("CARDNO", card)
	postData.QuerySetResult("VIP_LEVEL")
	s, err := api.post(postData)
	if err != nil {
		return ApiErrCode, err
	} else {
		result := GetBurgeonRowsArray(s)
		if len(result) == 0 {
			return -1, ErrNoDataFound
		}
		return gjson.Get(s, "0.rows.0.0").Int(), nil
	}
}

//获取VIP等级名称
func (api *BurgeonConnection) GetVipLevelNameByCard(card string) (string, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_VIP")
	postData.QuerySetCondition("CARDNO", card)
	postData.QuerySetResult("C_VIPTYPE_ID;NAME")
	s, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	} else {
		result := GetBurgeonRowsArray(s)
		if len(result) == 0 {
			return "", ErrNoDataFound
		}
		return gjson.Get(s, "0.rows.0.0").String(), nil
	}
}

func (api *BurgeonConnection) GetVipLevelUpMRetail(card string) (string, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_VIP")
	postData.QuerySetCondition("CARDNO", card)
	postData.QuerySetResult("M_RETAIL_ID;NAME")
	s, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	} else {
		result := GetBurgeonRowsArray(s)
		if len(result) == 0 {
			return "", ErrNoDataFound
		}
		return gjson.Get(s, "0.rows.0.0").String(), nil
	}
}

//获取Vip消费记录/返回消费金额,积分,日期[[2165,44,20210304]]
func (api *BurgeonConnection) GetVipRecordsOfConsumptionByCard(card string) ([][]int64, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("FA_VIPINTEGRAL_FTP")
	postData.QuerySetCondition("C_VIP_ID;CARDNO", card)
	postData.QuerySetResult("AMT_ACTUAL", "INTEGRAL", "CHANGDATE")
	s, err := api.post(postData)
	result := make([][]int64, 0)
	if err != nil {
		return result, err
	} else {
		array := GetBurgeonRowsArray(s)
		if len(array) == 0 {
			return nil, ErrNoDataFound
		}
		for _, v := range array {
			vv := v.Array()
			result = append(result, []int64{vv[0].Int(), vv[1].Int(), vv[2].Int()})
		}
		return result, nil
	}
}

//新增VIP
func (api *BurgeonConnection) CreateVip(cardinfo *VipCardConfig, card, mobil, name, birthday, sex, remark string) error {
	switch sex {
	case "M":
	case "W":
	case "男":
		sex = "M"
	case "女":
		sex = "W"
	default:
		sex = "N"
	}
	postData := api.NewObjectCreate()
	postData.ObjectCreateSetTable("C_V_ADDVIP")
	postData.ObjectCreateSetColumn("C_VIPTYPE_ID__NAME", cardinfo.VipType)
	postData.ObjectCreateSetColumn("CARDNO", card)
	postData.ObjectCreateSetColumn("C_CUSTOMER_ID__NAME", cardinfo.Customer)
	postData.ObjectCreateSetColumn("C_STORE_ID__NAME", cardinfo.Store)
	postData.ObjectCreateSetColumn("MOBIL", mobil)
	postData.ObjectCreateSetColumn("SEX", sex)
	postData.ObjectCreateSetColumn("VIPNAME", name)
	postData.ObjectCreateSetColumn("BIRTHDAY", birthday)
	postData.ObjectCreateSetColumn("C_INTEGRALAREA_ID__NAME", cardinfo.IntegralArea)
	postData.ObjectCreateSetColumn("OPENID", " ")
	postData.ObjectCreateSetColumn("DESCRIPTION", remark)
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

func (api *BurgeonConnection) CreateVipFromWeimob(cardinfo *VipCardConfig, card, mobil, remark string) error {
	postData := api.NewObjectCreate()
	postData.ObjectCreateSetTable("C_V_ADDVIP")
	postData.ObjectCreateSetColumn("C_VIPTYPE_ID__NAME", cardinfo.VipType)
	postData.ObjectCreateSetColumn("CARDNO", card)
	postData.ObjectCreateSetColumn("C_CUSTOMER_ID__NAME", cardinfo.Customer)
	postData.ObjectCreateSetColumn("C_STORE_ID__NAME", cardinfo.Store)
	postData.ObjectCreateSetColumn("MOBIL", mobil)
	postData.ObjectCreateSetColumn("VIPNAME", "微盟添加(未知)")
	postData.ObjectCreateSetColumn("SEX", "W")
	postData.ObjectCreateSetColumn("C_INTEGRALAREA_ID__NAME", cardinfo.IntegralArea)
	postData.ObjectCreateSetColumn("DESCRIPTION", remark)
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

//[{"message":"修改的记录数: 1","id":"3","code":0}]
//修改VIP生日
func (api *BurgeonConnection) ChangeVipBirthday(card string, birthday string) error {
	postData := api.NewObjectModify()
	postData.ObjectModifySetTable("c_vip")
	postData.ObjectModifySetColumn("ak", card)
	postData.ObjectModifySetColumn("birthday", birthday)
	postData.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

func (api *BurgeonConnection) ChangeVipName(card string, name string) error {
	postData := api.NewObjectModify()
	postData.ObjectModifySetTable("c_vip")
	postData.ObjectModifySetColumn("ak", card)
	postData.ObjectModifySetColumn("vipname", name)
	postData.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

func (api *BurgeonConnection) ChangeVipSex(card string, sex string) error {
	postData := api.NewObjectModify()
	postData.ObjectModifySetTable("c_vip")
	postData.ObjectModifySetColumn("ak", card)
	postData.ObjectModifySetColumn("sex", sex)
	postData.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

func (api *BurgeonConnection) ChangeVipWechatIfSub(card string, sub string) error {
	postData := api.NewObjectModify()
	postData.ObjectModifySetTable("c_vip")
	postData.ObjectModifySetColumn("ak", card)
	postData.ObjectModifySetColumn("is_wechat", sub)
	postData.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.post(postData)
	if err != nil {
		return err
	}
	return BurgeonApiResult(jsResponse)
}

//新增VIP积分调整单
func (api *BurgeonConnection) AddVipIntegral(card, billdate, remark string, integral int64) (string, error) {
	postData := api.NewProcessOrder()
	postData.ProcessOrderMasterObjSetTable("C_VIPINTEGRALADJ")
	postData.ProcessOrderIfSubmit(true)
	postData.ProcessOrderMasterObjSetColumn("BILLDATE", billdate)
	postData.ProcessOrderMasterObjSetColumn("ADJTYPE", 1)
	postData.ProcessOrderDetailObjsSetTables("C_VIPINTEGRALADJITEM")
	postData.ProcessOrderDetailObjsAddrefobjs("C_VIPINTEGRALADJITEM",
		map[string]interface{}{
			"C_VIP_ID__CARDNO": card,
			"INTEGRALADJ":      integral,
			"DESCRIPTION":      remark,
		})
	jsResponse, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	}

	if !gjson.Valid(jsResponse) {
		return EmptyString, fmt.Errorf(jsResponse)

	}
	//[{"message":"IJ2111250000001提交成功！！","id":"4","code":0,"objectid":31454}]
	return strings.ReplaceAll(gjson.Get(jsResponse, "0.message").String(), "提交成功！！", ""), nil

}

//按会员卡ID手机号新增查询VIP卡号
func (api *BurgeonConnection) GetVipCardNoByPhoneOp(phone string, viptype_id string) (string, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_VIP")
	postData.QuerySetConditionCombine("and")
	postData.QuerySetConditionExpr1("MOBIL", phone)
	postData.QuerySetConditionExpr2("C_VIPTYPE_ID", viptype_id)
	postData.QuerySetResult("CARDNO")
	jsResponse, err := api.post(postData)
	if err != nil {
		return EmptyString, err
	}
	if !gjson.Valid(jsResponse) {
		return EmptyString, fmt.Errorf(jsResponse)
	}
	dateString := gjson.Get(jsResponse, "0.rows.0.0").String()
	if dateString == "" {
		return "", errors.New("不存在该VIP")
	} else {
		return dateString, nil
	}

}

//企业微信调用

//获取VIP等级名称
func (api *BurgeonConnection) GetVipInfoByCardNo(card string) (string, string, string, error) {
	postData := api.NewQuery()
	postData.QuerySetTable("C_VIP")
	postData.QuerySetCondition("CARDNO", card)

	postData.QuerySetResult("CARDNO", "C_VIPTYPE_ID;NAME", "INTEGRAL")

	s, err := api.post(postData)
	if err != nil {
		return EmptyString, EmptyString, EmptyString, err
	}

	cardno := gjson.Get(s, "0.rows.0.0").String()
	cardname := gjson.Get(s, "0.rows.0.1").String()
	integral := gjson.Get(s, "0.rows.0.2").String()
	result := GetBurgeonRowsArray(s)
	if len(result) == 0 {
		return EmptyString, EmptyString, EmptyString, ErrNoDataFound
	}
	return cardno, cardname, integral, nil

}

func (api *BurgeonConnection) GetVipBuyInfoByCardNo(card string) (string, string, string, string, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("VIP最近消费查询")
	postData.ExecuteSqlSetValues(card)
	s, err := api.post(postData)
	if err != nil {
		return EmptyString, EmptyString, EmptyString, EmptyString, err
	} else {
		result := gjson.Get(s, "0.result").Array()
		if len(result) == 0 {
			return EmptyString, EmptyString, EmptyString, EmptyString, ErrNoDataFound
		}
		m_id := gjson.Get(s, "0.result.0.0").String()
		billdate := gjson.Get(s, "0.result.0.1").String()
		storename := gjson.Get(s, "0.result.0.2").String()
		allprice := gjson.Get(s, "0.result.0.3").String()
		return m_id, billdate, storename, allprice, nil
	}

}

//零售单价ID
func (api *BurgeonConnection) GetVipBuySkuByCardNo(m_id string) (string, string, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("VIP最近消费SKU查询")
	postData.ExecuteSqlSetValues(m_id)
	s, err := api.post(postData)
	if err != nil {
		return EmptyString, EmptyString, err
	} else {
		result := gjson.Get(s, "0.result").Array()
		if len(result) == 0 {
			return EmptyString, EmptyString, ErrNoDataFound
		}
		product := gjson.Get(s, "0.result.0.0").String()
		name := gjson.Get(s, "0.result.0.1").String()

		return product, name, nil
	}

}

func (api *BurgeonConnection) GetVipByBuyDate(date string) ([]gjson.Result, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("获取某一天VIP消费")
	postData.ExecuteSqlSetValues(date)
	s, err := api.post(postData)
	if err != nil {
		return nil, err
	} else {
		vips := gjson.Get(s, "0.result").Array()
		if len(vips) == 0 {
			return nil, ErrNoDataFound
		}
		return vips, nil
	}
}

func (api *BurgeonConnection) GetVipByInfoWithBuyDate(sdate, edate string) ([]gjson.Result, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("VIP某日到店消费明细")
	postData.ExecuteSqlSetValues(sdate)
	postData.ExecuteSqlSetValues(edate)
	s, err := api.post(postData)
	if err != nil {
		return nil, err
	} else {
		vips := gjson.Get(s, "0.result").Array()
		if len(vips) == 0 {
			return nil, ErrNoDataFound
		}
		return vips, nil
	}
}

func (api *BurgeonConnection) GetVipByInfoWithBuyDate2(sdate, edate string) ([]gjson.Result, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("VIP某日到店消费明细2")
	postData.ExecuteSqlSetValues(sdate)
	postData.ExecuteSqlSetValues(edate)
	s, err := api.post(postData)
	if err != nil {
		return nil, err
	} else {
		vips := gjson.Get(s, "0.result").Array()
		if len(vips) == 0 {
			return nil, ErrNoDataFound
		}
		return vips, nil
	}
}

func (api *BurgeonConnection) GetSalersNo(id string) (string, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("ID获取员工工号")
	postData.ExecuteSqlSetValues(id)
	s, err := api.post(postData)
	if err != nil {
		return "", err
	} else {
		no := gjson.Get(s, "0.result.0").String()
		if len(no) == 0 {
			return "", ErrNoDataFound
		}
		return no, nil
	}
}

//更新零售单据VIP
func (api *BurgeonConnection) UpdateRetailVip(docno, cardno string) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("更新零售单据VIP")
	postData.ExecuteSqlSetValues(docno)
	postData.ExecuteSqlSetValues(cardno)
	s, err := api.post(postData)
	fmt.Println(s, err)
}
