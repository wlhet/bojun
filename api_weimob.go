package bojun

import (
	"fmt"
	"strconv"

	"github.com/tidwall/gjson"
)

//专用于对接weimob
//获取未处理的VIP积分明细
func (api *BurgeonConnection) GetIntegralItems() ([]gjson.Result, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("获取未同步微盟的积分明细")
	s, err := api.post(postData)
	if err != nil {
		return nil, err
	} else {
		res := gjson.Get(s, "0.result").Array()
		if len(res) == 0 {
			return nil, ErrNoDataFound
		}
		return res, nil
	}
}

//专用于对接weimob
//获取未处理的VIP积分明细
func (api *BurgeonConnection) UpdateIntegralItems(rid int64) error {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("更新已同步微盟的积分明细")
	id := strconv.Itoa(int(rid))
	postData.ExecuteSqlSetValues(id)
	s, err := api.post(postData)
	if err != nil {
		return err
	} else {
		code := gjson.Get(s, "code").Int()
		if code == 0 {
			return nil
		} else {
			return fmt.Errorf(gjson.Get(s, "message").String())
		}
	}
}

func (api *BurgeonConnection) GetAllWeiMobStoreInfo() ([]gjson.Result, error) {
	postData := api.NewExecuteSql()
	postData.ExecuteSqlSetName("获取微盟店铺")
	s, err := api.post(postData)
	if err != nil {
		return nil, err
	} else {
		res := gjson.Get(s, "0.result").Array()
		if len(res) == 0 {
			return nil, ErrNoDataFound
		}
		return res, nil
	}
}

func (api *BurgeonConnection) WebActionWithId(name string, id int64) error {
	postData := api.NewWebActionWithId(name, id)
	s, err := api.post(postData)
	if err != nil {
		return err
	} else {
		code := gjson.Get(s, "0.code").Int()
		if code != 0 {
			return fmt.Errorf(gjson.Get(s, "0.message").String())
		}
		return nil
	}
}

func (api *BurgeonConnection) WebActionWithAk(name string, ak string) error {
	postData := api.NewWebActionWithAk(name, ak)
	s, err := api.post(postData)
	if err != nil {
		return err
	} else {
		code := gjson.Get(s, "0.code").Int()
		if code != 0 {
			return fmt.Errorf(gjson.Get(s, "0.message").String())
		}
		return nil
	}
}
func (api *BurgeonConnection) ObjectSubmitWithId(table string, id int64) error {
	postData := api.NewObjectSubmitWithId(table, id)
	s, err := api.post(postData)
	if err != nil {
		return err
	} else {
		code := gjson.Get(s, "0.code").Int()
		if code != 0 {
			return fmt.Errorf(gjson.Get(s, "0.message").String())
		}
		return nil
	}
}

func (api *BurgeonConnection) ObjectSubmitWithAk(table string, ak string) error {
	postData := api.NewObjectSubmitWithAk(table, ak)
	s, err := api.post(postData)
	if err != nil {
		return err
	} else {
		code := gjson.Get(s, "0.code").Int()
		if code != 0 {
			return fmt.Errorf(gjson.Get(s, "0.message").String())
		}
		return nil
	}
}
