package bojun

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/tidwall/gjson"

	"gitee.com/wlhet/httplib"
)

// var NewVipCardInfo *VipCardConfig

type BurgeonConnection struct {
	AppId  string
	AppKey string
	ApiUrl string
}

type PostData struct {
	Id      int                    `json:"id"`      //id
	Command string                 `json:"command"` //操作类型
	Params  map[string]interface{} `json:"params"`  //所需参数  json
}

//创建对象
type ObjectCreatePostData PostData

//创建对象
type ObjectGetPostData PostData

//修改对象
type ObjectModifyPostData PostData

//提交对象
type ObjectSubmit PostData

//执行任务
type ProcessOrderPostData PostData

//查询
type QueryPostData PostData

//用于初始化包内默认连接对象
// func InitBurgeonConnection(AppId, AppKey, ApiUrl string) {
// 	baseConnection = &BurgeonConnection{
// 		AppId:  AppId,
// 		AppKey: AppKey,
// 		ApiUrl: ApiUrl,
// 	}
// }

// func (bg *BurgeonConnection) InitVipCardConfig(viptype, customer, store, area, validdate string) {
// 	NewVipCardInfo = &VipCardConfig{
// 		VipType:      viptype,
// 		Customer:     customer,
// 		Store:        store,
// 		IntegralArea: area,
// 		ValidDate:    validdate,
// 	}
// }

//Burgeon--------------------------------------------------------------------------
//APP_KEY_2 + tmp + app_ser_md5s
//将密码MD5后和账号时间戳按照 账号 时间戳 密码MD5 拼接 返回
func (bg *BurgeonConnection) getSign() (string, string) {
	tmp := bg.millisecondString()
	md5key := StringToMd5(bg.AppKey)
	return tmp, StringToMd5(bg.AppId + tmp + md5key)
}

//2020-09-26 15:06:23.000
func (bg *BurgeonConnection) millisecondString() string {
	tmp := time.Now().Format("2006-01-02 15:04:05.000")
	return tmp
}

//封装了请求提交函数  只需传入 transactions即可  返回json字符串
func (bg *BurgeonConnection) post(comm ...interface{}) (string, error) {
	dataJsonByte, _ := json.Marshal(&comm)
	dataJsonStr := string(dataJsonByte)
	tmp, sign := bg.getSign()

	req := httplib.Post(bg.ApiUrl)
	req.Param("sip_appkey", bg.AppId)
	req.Param("sip_timestamp", tmp)
	req.Param("sip_sign", sign)
	req.Param("transactions", dataJsonStr)
	response, err := req.String()
	if err != nil {
		BurgeonLog.Info("\n调用伯俊Api失败, 错误信息:   %s\n", err.Error())
		return EmptyString, err
	}

	if !gjson.Valid(response) {
		BurgeonLog.Info("\n调用伯俊Api失败, 错误信息:   %s\n", response)
		return EmptyString, errors.New(response)
	}
	jsParse := gjson.Parse(response)
	code := jsParse.Get("0.code").Int()
	errmsg := jsParse.Get("0.message").String()
	if code == 0 {
		BurgeonLog.Info("\n调用伯俊Api成功,返回结果正确\n请求参数:   %s\n返回数据:   %s\n", dataJsonStr, response)
		return response, nil
	} else {
		BurgeonLog.Info("\n调用伯俊Api成功,返回结果错误\n请求参数:   %s\n返回数据:   %s\n", dataJsonStr, response)
		return EmptyString, errors.New(errmsg)
	}
}
