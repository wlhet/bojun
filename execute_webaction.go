package bojun

//执行页面按钮事件

func (this *BurgeonConnection) NewWebActionWithId(name string, id int64) PostData {
	return PostData{Id: 6, Command: "ExecuteWebAction", Params: map[string]interface{}{
		"webaction": name,
		"id":        id,
	}}
}

func (this *BurgeonConnection) NewWebActionWithAk(name string, ak string) PostData {
	return PostData{Id: 6, Command: "ExecuteWebAction", Params: map[string]interface{}{
		"webaction": name,
		"ak":        ak,
	}}
}
