module gitee.com/wlhet/bojun

go 1.17

require (
	gitee.com/wlhet/httplib v0.0.6
	github.com/astaxie/beego v1.12.3
	github.com/tidwall/gjson v1.12.1
)

require (
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)
