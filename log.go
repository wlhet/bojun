package bojun

import "github.com/astaxie/beego/logs"

// var baseConnection *BurgeonConnection
var BurgeonLog *logs.BeeLogger

func init() {
	BurgeonLog = logs.NewLogger(1000) // 创建一个日志记录器，参数为缓冲区的大小
	BurgeonLog.SetLogger(logs.AdapterFile, `{"filename":"log/burgeonApi.log"}`)
	BurgeonLog.SetLevel(logs.LevelDebug) // 设置日志写入缓冲区的等级
	BurgeonLog.EnableFuncCallDepth(true) // 输出log时能显示输出文件名和行号（非必须）
}
