package bojun

func (this *BurgeonConnection) NewObjectCreate() ObjectCreatePostData {
	return ObjectCreatePostData{Id: 2, Command: "ObjectCreate", Params: make(map[string]interface{})}
}

//设置查询的表
func (this *ObjectCreatePostData) ObjectCreateSetTable(t string) {
	this.Params["table"] = t
}

//设置返回结果
func (this *ObjectCreatePostData) ObjectCreateSetColumn(k string, v interface{}) {
	this.Params[k] = v
}
