package bojun

func (this *BurgeonConnection) NewObjectGet() ObjectGetPostData {
	return ObjectGetPostData{Id: 8, Command: "GetObject", Params: make(map[string]interface{})}
}

//设置查询的表
func (this *ObjectGetPostData) ObjectGetSetTable(t string) {
	this.Params["table"] = t
}

//设置表ID
func (this *ObjectGetPostData) ObjectGetSetObjectId(id int64) {
	this.Params["id"] = id
}

//设置子表
func (this *ObjectGetPostData) ObjectGetSetRefTables(id int64) {
	ref, ok := this.Params["reftables"].([]int64)
	if ok {
		ref = append(ref, id)
		this.Params["reftables"] = ref
	} else {
		ref := make([]int64, 0)
		ref = append(ref, id)
		this.Params["reftables"] = ref
	}

}
