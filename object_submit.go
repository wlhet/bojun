package bojun

//提交

func (this *BurgeonConnection) NewObjectSubmitWithId(table string, id int64) ObjectSubmit {
	return ObjectSubmit{Id: 7, Command: "ObjectSubmit", Params: map[string]interface{}{
		"table": table,
		"id":    id,
	}}
}

func (this *BurgeonConnection) NewObjectSubmitWithAk(table string, ak string) ObjectSubmit {
	return ObjectSubmit{Id: 7, Command: "ObjectSubmit", Params: map[string]interface{}{
		"table": table,
		"ak":    ak,
	}}
}
