package bojun

func (this *BurgeonConnection) NewProcessOrder() ProcessOrderPostData {
	return ProcessOrderPostData{Id: 4, Command: "ProcessOrder", Params: map[string]interface{}{
		"submit":    false,
		"id":        -1,
		"masterobj": make(map[string]interface{}),
		"detailobjs": map[string]interface{}{
			"tables":  make([]string, 0),
			"refobjs": make([]map[string]interface{}, 0),
		},
	}}
}

//存储过程单据是否提交
func (this *ProcessOrderPostData) ProcessOrderIfSubmit(b bool) {
	this.Params["submit"] = b
}

//设置主表名称
func (this *ProcessOrderPostData) ProcessOrderMasterObjSetTable(table string) {
	masterobj := this.Params["masterobj"].(map[string]interface{})
	masterobj["table"] = table
}

//设置主表字段
func (this *ProcessOrderPostData) ProcessOrderMasterObjSetColumn(k string, v interface{}) {
	masterobj := this.Params["masterobj"].(map[string]interface{})
	masterobj[k] = v
}

//子表名称 可是复数  多表
func (this *ProcessOrderPostData) ProcessOrderDetailObjsSetTables(t ...string) {
	detailobjs := this.Params["detailobjs"].(map[string]interface{})
	tables := detailobjs["tables"].([]string)
	tables = append(tables, t...)
	detailobjs["tables"] = tables
}

//设置子表提交数据   表名  各个字段
func (this *ProcessOrderPostData) ProcessOrderDetailObjsAddrefobjs(reftable string, list ...map[string]interface{}) {
	detailobjs := this.Params["detailobjs"].(map[string]interface{})
	refobjs := detailobjs["refobjs"].([]map[string]interface{})
	detailobjs["refobjs"] = append(refobjs, map[string]interface{}{"table": reftable, "addList": list})
}
